<?php

namespace App\Model;

use Nette,
	Nette\Utils\Image;

/**
 * Wishes model.
 */
define("constUserData", __DIR__."/../../userData/");
define("constImages", __DIR__."/../../www/images/upload/");

class FileManager extends Nette\Object {
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}
	public function getUserDataPWD(){
		return constUserData;
	}
	public function saveFileToLocation($constType,$path,$file,$fileType,$userID,$source = null){
		if ($constType=="userData") {
			$dir = constUserData;
		}
		else if ($constType=="images") {
			$dir = constImages;
		}

		$hash = hash("gost",$file->name.date("now"));

        if ($file->isOk()) {
        	$fullPath = $dir.$path."/".$userID;
        	if (!file_exists($fullPath)) {
        		mkdir($fullPath);
        	}
        	$fullPath = $fullPath."/".$hash.".".$fileType;


        	if(!$source){
        		$file->move($fullPath, 100, Image::PNG);
        	}else{
	       	 	copy($source,$fullPath);
        	}
        }	

        return array('path' => $fullPath,'hash' => $hash );		
    }
					
	

}