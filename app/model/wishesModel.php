<?php

namespace App\Model;

use Nette;

/**
 * Wishes model.
 */
class wishesModel extends Nette\Object {
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	public function getAll() {
		return $this->database->table('wishes');
	}

	public function allWishesFromFriends($id, $offset = 0, $limit = 10) {
		return $this->database->query('SELECT w.*,u.name as username,u.surname,u.profile_image FROM wishes as w LEFT JOIN friends as f ON w.user_id=f.friend_id LEFT JOIN users as u ON u.id=f.friend_id where f.user_id=? order by created desc LIMIT ?,?', $id, $offset, $limit);
	}
	public function countAllWishesFromFriends($id) {
		return $this->database->query('SELECT count(*) as count FROM wishes as w LEFT JOIN friends as f ON w.user_id=f.friend_id where f.user_id=? order by created desc ', $id);
	}
	public function allFromUser($id) {
		return $this->getAll()->where('user_id', $id);
	}

	public function getImageAndTitle($url) {
		return \OpenGraph::fetch($url);
	}

}