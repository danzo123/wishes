<?php
namespace App\Presenters;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter {
	public $id;

	public function startup() {
		if ($this->users->getAll()->count()) {
			parent::startup();
			if (!$this->user->isLoggedIn()) {
				$this->redirect("Sign:in");
			} else {
				if ($this->userInfo->name == "") {
					$this->flashMessage($this->translator->translate("messages.flashes.firstsettings"));
					$this->redirect("Settings:");
				} else {
					$this->template->userDetails = $this->userInfo;
					$this->template->name = $this->userInfo->name . " " . $this->userInfo->surname;
				}

			}

		} else {
			$this->redirect("Sign:in");
		}
	}

	public function actionDefault($id = null) {
		/*echo "<pre>";
		var_dump($this->wishesModel->countAllWishesFromFriends($this->user->id)->fetch()->count
);
		exit;*/
		$this->template->wishes = $this->wishesModel->allWishesFromFriends($this->user->id, 0, 10);
		if ($this->wishesModel->CountAllWishesFromFriends($this->user->id)->fetch()->count <= 10) {
			$this->template->isButton = false;
		}else{
			$this->template->isButton = true;
		}

	}

	public function handleNextFeed($offset) {
		$maxLimit = $this->wishesModel->CountAllWishesFromFriends($this->user->id)->fetch()->count;

		if ($offset < $maxLimit) {
			if (($offset + 10) >= $maxLimit) {
				$this->payload->isButton = 0;
			} else {
				$this->payload->isButton = 1;
			}

			$wishes = $this->wishesModel->allWishesFromFriends($this->user->id, intval($offset));
			$string = "";
			foreach ($wishes as $w) {
				$string .= '<div class="row">
								<div class="col s12 m12 l12">
									<div class="card feed">
										<div class="card-content row" style="border-bottom:1px solid rgba(160, 160, 160, 0.2); margin: 15px !important">
											<p style="float:right">
												' . $w->created . '
											</p>
											<p class="homepagePhoto">';
				if ($w->profile_image != "") {
					$string .= '<img class="responsive-img" src="\..\..\images\upload\profile/' . $w->profile_image . '.png" />';
				} else {
					$string .= '<img class="responsive-img" src="\..\..\images\profile.png" />';
				}

				$string .= '</p>
											<p id="' . $w->id . '">
												' . $w->username . ' ' . $w->surname . '
											</p>
											<p id="' . $w->id . '">
												' . $w->name . '
											</p>
										</div>
										<div class="card-image col l4 offset-l4" style="padding: 15px;">';
				if ($w->image) {
					$string .= '<img class="responsive-img" src="/../../images/upload/wishes/' . $w->image . '.png"/>';
				} else {
					$string .= '<img class="responsive-img" src="/../../images/gift.png"/>';
				}

				$string .= '</div>
										<div class="card-action col l12">
											<a href="' . $w->link . '">Description</a>
										</div>
									</div>
								</div>
							</div>';
			}
			$this->payload->string = $string;
			$this->terminate();
		}
	}
}
