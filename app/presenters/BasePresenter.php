<?php

namespace App\Presenters;

use App\Model;
use Kdyby\Translation;
use Nette;
use Symfony\Component\Translation\Translator;
use Nette\Application\UI;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {
	/** @var \Kdyby\Translation\Translator @inject */
	public $translator;

	/**
	 * @var \App\Model\UserManager
	 * @inject
	 */
	public $users;

	/**
	 * @var \App\Model\Arrays
	 * @inject
	 */
	public $arrays;

	/**
	 * @var \App\Model\wishesModel
	 * @inject
	 */
	public $wishesModel;

	/**
	 * @var \App\Model\FileManager
	 * @inject
	 */
	public $fileManager;


	public $userInfo;

	public function startup() {
		parent::startup();
		$this->userInfo = $this->users->byId($this->user->id)->fetch();
		if (filemtime("../www/css/style.less") > filemtime("../www/css/style.css")) {
			//include_once __DIR__ . '/../../vendor/others/lessc.inc.php';
			$less = new \lessc;
			$less->compileFile("../www/css/style.less", "../www/css/style.css");
		}

		if (isset($_COOKIE["lang"])) {
			$this->translator->setLocale($_COOKIE["lang"]);
		} else {
			$this->translator->setLocale($this->translator->getLocale());
		}
		// $this->translator->setLocale("en");
	}

	public function handleChangeLanguage($l) {
		setcookie("lang", $l, time() + (86400 * 9999999), "/");
		$this->redirect('this');
	}

	public function getUrlCharset($url) {
		$headers = get_headers($url);
		foreach ($headers as $h) {
			if (strpos($h, 'charset') !== false) {
				$result = $h;
				break;
			}
		}
		$result = explode(";", $result);
		$result = explode("=", $result[1]);

		return $result[1];
	}

	public function handleOverlay($show, $snippet) {
		$this->template->overlay = $show;

		if ($this->isAjax()) {
			$this->redrawControl($snippet);
		}
	}

	public function createComponetSearchForm() {
		$form = new UI\Form;
        ///$form->;
	}
	public function handleSearch($value='')
	{
		$friendArray = array();
		$notfriendArray = array();
		$arrayFull = array();
		$temp = array();


		foreach($this->users->searchForUsers($value) as $x){
		$friend = false;
		if(count($this->users->getIfFriended($this->user->id,$x->id))||count($this->users->getIfFriended($x->id,$this->user->id))){
			$friend = true;
		}
			if($x->profile_image){
				$temp = array(
					"icon" => "<img height=20px src=\"/images/upload/profile/".$x->id."/".$x->profile_image.".png\">",
					"label" => $x->name." ".$x->surname,"id"=>$x->id);
			}else{
				$temp = array(
					"icon" => "<img height=20px src=\"/images/profile.png\">",
					"label"=>$x->name." ".$x->surname,
					"id"=>$x->id);
			}
			if($friend){
				array_push($friendArray, $temp);
			}else{
				array_push($notfriendArray, $temp);
			}
		}
		$arrayFull= array_merge($friendArray,$notfriendArray);
		$this->payload->items = $arrayFull;
		$this->terminate();

	}
	

}
