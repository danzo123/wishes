<?php
namespace App\Presenters;
// include_once('..\vendor\others\simple_html_dom.php');
use Nette;
use Nette\Utils\Image;

class ProfilePresenter extends BasePresenter {

	public $edit;
	public $id;
	private $wishes;

	public function startup($id = null) {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->redirect("Sign:in");
		} else {
			if ($this->userInfo->name == "") {
				$this->flashMessage($this->translator->translate("messages.flashes.firstsettings"));
				$this->redirect("Settings:");
			}
		}
	}

	public function actionDefault($id = null) {
		// $graph = \OpenGraph::fetch('http://www.alza.sk/ipad-mini-3-s-retina-displejem-16gb-gold-d2220848.htm');
		// echo "<pre>";
		// var_dump($graph);
		// exit;
		if (!$id) {
			$id = $this->user->id;
		}

		$this->wishes = $this->template->wishes = $this->wishesModel->allFromUser($id)->order("created DESC");
		$this->template->userDetails = $this->userInfo = $this->users->byId($id)->fetch();
		if (!$this->userInfo) {
			$this->flashMessage("user does not exist");
			$this->redirect("profile:");
		}
		//$this->template->wishes = $this->wishesModel->allFromUser($id)->where("position", 0)->order("created DESC");
		// $this->template->wishes = $this->wishes->where("position > ?", 1)->order("position ASC");
		$this->template->following = $this->users->getFollowing($id)->where("user_id", $id);
		$this->template->followers = $this->users->getFollowers($id)->where("friend_id", $id);

		$this->template->overlay = 0;
	}

	public function createComponentAddWishForm() {
		$form = new Nette\Application\UI\Form;

		$form->addText('name', $this->translator->translate("Name *"))
		     ->setRequired($this->translator->translate("Please, fill name"));

		$form->addText('link', $this->translator->translate("Link"));

		$form->addUpload('image', $this->translator->translate("Image"))
		     ->setAttribute("onchange", "readURL(this);");

		$form->addSubmit('send', $this->translator->translate("Save"))
		     ->setAttribute("class", "btn button-green col s12 m12 l12");

		$form->onSuccess[] = $this->addWishFormSucceeded;

		return $form;
	}

	public function addWishFormSucceeded($form, $values) {
		$hash = "";
		//////////////img upload
		// echo "<pre>";
		// var_dump($values->image);exit;
		if ($values->image->size != null) {
			$file = $values->image;
			$image = Image::fromFile($file);
			$hash = hash("gost", $values->image->name . date("now"));
			if ($file->isOk()) {
				$path = getcwd() . "/images/upload/wishes/" . $hash . '.png';
				$image->save($path, 100, Image::PNG);
			}
		} else {
			$graph = $this->wishesModel->getImageAndTitle($values->link);
			if ($graph) {
				$image = $graph->image;
				if (isset($image)) {
					$hash = hash("gost", $values->link . date("now"));
					$path = getcwd() . "/images/upload/wishes/" . $hash . '.png';
					$imageLink = str_replace("&amp;", "&", $image);
					file_put_contents($path, file_get_contents($imageLink));
				}
			}

		}
		///////////img upload end
		try {
			$this->wishesModel->getAll()->insert(array(
				"name" => $values->name,
				"link" => $values->link,
				"image" => $hash,
				"user_id" => $this->user->id,
			));
			$this->flashMessage("wish bol ulozeny");
		} catch (Nette\Security\AuthenticationException $e) {
			$this->flashMessage($e->getMessage());
		}
		$this->redirect('Profile:');
	}

	// public function handleSaveOrder($array) {
	// 	$array = explode(',', $array);
	// 	unset($array[0]);
	// 	foreach ($array as $position => $id) {
	// 		$this->wishesModel->getAll()->where("id", $id)->update(array("position" => $position));
	// 	}
	// }

	public function handleDeleteWish($delete_id) {
		if ($this->user->id == $this->userInfo->id && $this->user->isLoggedIn()) {
			$this->wishesModel->getAll()->where('id', $delete_id)->delete();
			if ($this->isAjax()) {
				$this->redrawControl('wishe');
			}

		} else {
			$this->flashMessage($this->translator->translate("Nemas opravnenie mazat"));
			$this->redirect("this");
		}
	}

	// public function handleEditWish($id){
	// 	$this->edit = true;
	// 	if($this->isAjax())
	// 		$this->redrawControl("wish".$id);
	// }

	public function isFriend($friend_id) {
		return $this->users->getFriends()->where(array("friend_id" => $friend_id, "user_id" => $this->user->id))->count();
	}

	public function handleAddFriend($friend_id) {
		$this->users->getFriends()->insert(array(
			"user_id" => $this->user->id,
			"friend_id" => $friend_id,
		));
		if ($this->isAjax()) {
			$this->redrawControl("profile");
		}
	}

	public function handleSortWishes($type) {
		if (!$this->id) {
			$wishes = $this->wishesModel->allFromUser($this->user->id);
		} else {
			$wishes = $this->wishesModel->allFromUser($this->id);
		}

		if ($type == "date") {
			$this->template->wishes = $wishes->order("created ASC");
		} elseif ($type == "price") {
			$this->template->wishes = $wishes->order("price ASC");
		} else {
			$this->template->wishes = $wishes->order("name ASC");
		}

		if ($this->isAjax()) {
			$this->redrawControl("wishe");
		}

	}

	public function handleGetImageAndName($url) {
		$tags = $this->wishesModel->getImageAndTitle($url);
		//var_dump($tags);exit;
		//$title = mb_convert_encoding($tags->title, "UTF-8");
		$image = $tags->image;
		$this->payload->message = array('title' => $tags->title, 'image' => $image);
		$this->terminate();
	}

	public function handleValidUrl($url) {
		$file_headers = @get_headers($url);
		if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == "HTTP/1.0 301 Moved Permanently" || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
			$this->payload->valid = false;
			$this->terminate();
		} else {
			$this->payload->valid = true;
			$this->terminate();
		}
	}
}